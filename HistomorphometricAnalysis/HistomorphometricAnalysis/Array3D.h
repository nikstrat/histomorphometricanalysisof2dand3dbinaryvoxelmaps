#pragma once

#include <assert.h>
#include "BoundingBox.h"


/* -------------------------------------------------------------------------- */
/* Basic element types.                                                       */
/* -------------------------------------------------------------------------- */
enum class ELEMENT {
	ELEMENT_EMPTY,
	ELEMENT_FILLED,
	ELEMENT_VARIOUS,
	ELEMENT_NUMTYPES
};

/* -------------------------------------------------------------------------- */
/* Array3D class.                                                             */
/* -------------------------------------------------------------------------- */
class Array3D
{
private:
	ELEMENT* Array{};
	size_t      DimX{}, DimY{}, DimZ{}, PlaneXY{};
	//size_t      TotalElements{};

public:
	inline Array3D(const size_t DX, const size_t DY, const size_t DZ) noexcept;//constructor
	inline Array3D(const Array3D& other) noexcept;//copy constructor
	inline ~Array3D() noexcept;//destructor
	inline size_t GetDimX() const noexcept;
	inline size_t GetDimY() const noexcept;
	inline size_t GetDimZ() const noexcept;
	inline ELEMENT GetElement(const size_t x, const size_t y, const size_t z) const noexcept;
	inline ELEMENT GetElement(const size_t elm)const noexcept;
	inline void SetElement(const size_t x, const size_t y, const size_t z, const ELEMENT elm) noexcept;
	inline size_t CountElements(const ELEMENT& elm) const noexcept;
	inline ELEMENT FindElementTypes() const noexcept;
	inline size_t CountElements(const ELEMENT& elm, const size_t z) const noexcept;
	inline ELEMENT FindElementTypes(const size_t z) const noexcept;
	inline void SetElements(const ELEMENT& elm, const size_t z) noexcept;
	inline size_t CountElements(const ELEMENT& elm, const BoundingBox2D& BB, const size_t z) const noexcept;
	inline ELEMENT FindElementTypes(const BoundingBox2D& BB, const size_t z) const noexcept;
	inline size_t CountElements(const ELEMENT& elm, const BoundingBox3D& BB) const noexcept;
	inline ELEMENT FindElementTypes(const BoundingBox3D& BB) const noexcept;
	inline size_t GetTotalElements() const noexcept;
	//inline size_t GetTotalFilledElements() const noexcept;
	inline BoundingBox3D GetBoundingBox3D() const noexcept;
	void PrintArray2D(const BoundingBox2D& BB, const size_t z) const noexcept;
	//void PrintArray3D(const BoundingBox3D &BB) const noexcept;
	//void PrintArray3D(const BoundingBox3D &BB, const size_t z) const noexcept;
	bool LoadFromFile(const char* FileSpec) noexcept;
	bool SaveToFile(const char* FileSpec) noexcept;
	inline void InitArray3D(const ELEMENT& elm) noexcept;
	inline void InitArray3D(const BoundingBox3D& BB, const ELEMENT& elm) noexcept;
	inline const Array3D& operator=(const Array3D& op2) noexcept;
	inline bool operator==(const Array3D& op2) const noexcept;
	inline void Reset() noexcept;

};



//--- IMPLEMENTATION ---//


Array3D::Array3D(const size_t DX, const size_t DY, const size_t DZ) noexcept
	: DimX(DX), DimY(DY), DimZ(DZ), PlaneXY(DX* DY)
{
	Array = (ELEMENT*)malloc(sizeof(ELEMENT) * DimZ * DimY * DimX);//3d array memory allocation
	assert(Array != nullptr);

	for (size_t k = 0; k < DimZ; k++) // DIMZ
	{
		for (size_t j = 0; j < DimY; j++) // DIMY
		{
			for (size_t i = 0; i < DimX; i++) // DIMX
			{
				//Array[k * DimY * DimX + j * DimX + i] = ELEMENT_EMPTY;
				Array[k * PlaneXY + j * DimX + i] = ELEMENT::ELEMENT_EMPTY;
			}	// for x
		}	// for y
	}	// for z
}	// Constructor

Array3D::Array3D(const Array3D& other) noexcept
{
	if (this != &other)
	{
		size_t count;

		DimX = other.DimX;
		DimY = other.DimY;
		DimZ = other.DimZ;
		PlaneXY = other.PlaneXY;

		Array = (ELEMENT*)malloc(sizeof(ELEMENT) * DimZ * DimY * DimX);
		if (Array != nullptr)
		{
			for (count = 0; count < DimX * DimY * DimZ; count++)
			{
				Array[count] = other.Array[count];
			}	// for
		}//if
	}	// if
}	// copy constructor

Array3D::~Array3D() noexcept
{
	free(Array);
	Array = nullptr;
	DimX = DimY = DimZ = PlaneXY = 0;
}	// Destructor

inline size_t Array3D::GetDimX() const noexcept { return DimX; }
inline size_t Array3D::GetDimY() const noexcept { return DimY; }
inline size_t Array3D::GetDimZ() const noexcept { return DimZ; }

inline ELEMENT Array3D::GetElement(const size_t x, const size_t y, const size_t z) const noexcept
{
	//return Array[z * DimY * DimX + y * DimX + x];
	return Array[z * PlaneXY + y * DimX + x];
}	// GetElement

inline ELEMENT Array3D::GetElement(const size_t elm)const noexcept
{
	return Array[elm];
}

inline void Array3D::SetElement(const size_t x, const size_t y, const size_t z, const ELEMENT elm) noexcept
{
	//Array[z * DimY * DimX + y * DimX + x] = elm;
	Array[z * PlaneXY + y * DimX + x] = elm;
}	// SetElement

inline size_t Array3D::CountElements(const ELEMENT& elm) const noexcept
{
	size_t	Counter = 0, i, j, k;

	for (k = 0; k < DimZ; k++)
	{
		for (j = 0; j < DimY; j++)
		{
			for (i = 0; i < DimX; i++)
			{
				//if ( Array[k * DimY * DimX + j * DimX + i] == elm )
				if (Array[k * PlaneXY + j * DimX + i] == elm)
				{
					Counter++;
				}	// if
			}	// for x
		}	// for y
	}	// for z

	return Counter;
}	// CountElements

inline ELEMENT Array3D::FindElementTypes() const noexcept
{
	size_t	i, j, k;
	ELEMENT	Elm;
	bool	HasEmpty = false, HasFilled = false;

	for (k = 0; k < DimZ; k++)
	{
		for (j = 0; j < DimY; j++)
		{
			for (i = 0; i < DimX; i++)
			{
				Elm = GetElement(i, j, k);
				HasEmpty = HasEmpty || (Elm == ELEMENT::ELEMENT_EMPTY);
				HasFilled = HasFilled || (Elm == ELEMENT::ELEMENT_FILLED);

				// Terminate prematurely of there is more than one element type.
				if (HasFilled && HasEmpty) return ELEMENT::ELEMENT_VARIOUS;
			}	// for x
		}	// for y
	}	// for z

	if (!HasFilled) return ELEMENT::ELEMENT_EMPTY;
	/*if ( !HasEmpty )*/ return ELEMENT::ELEMENT_FILLED;
}	// FindElementTypes

inline size_t Array3D::CountElements(const ELEMENT& elm, const size_t z) const noexcept
{
	size_t	Counter = 0, i, j;

	for (j = 0; j < DimY; j++)
	{
		for (i = 0; i < DimX; i++)
		{
			//if ( Array[z * DimY * DimX + j * DimX + i] == elm )
			if (Array[z * PlaneXY + j * DimX + i] == elm)
			{
				Counter++;
			}	// if
		}	// for x
	}	// for y

	return Counter;
}	// CountElements

inline ELEMENT Array3D::FindElementTypes(const size_t z) const noexcept
{
	size_t	i, j;
	ELEMENT	Elm;
	bool	HasEmpty = false, HasFilled = false;

	for (j = 0; j < DimY; j++)
	{
		for (i = 0; i < DimX; i++)
		{
			Elm = GetElement(i, j, z);
			HasEmpty = HasEmpty || (Elm == ELEMENT::ELEMENT_EMPTY);
			HasFilled = HasFilled || (Elm == ELEMENT::ELEMENT_FILLED);

			// Terminate prematurely of there is more than one element type.
			if (HasFilled && HasEmpty) return ELEMENT::ELEMENT_VARIOUS;
		}	// for x
	}	// for y

	if (!HasFilled) return ELEMENT::ELEMENT_EMPTY;
	/*if ( !HasEmpty )*/ return ELEMENT::ELEMENT_FILLED;
}	// FindElementTypes 

inline void Array3D::SetElements(const ELEMENT& elm, const size_t z) noexcept
{
	size_t	i, j;

	for (j = 0; j < DimY; j++)
	{
		for (i = 0; i < DimX; i++)
		{
			Array[z * PlaneXY + j * DimX + i] = elm;
		}	// for x
	}	// for y
}	// SetElements

inline size_t Array3D::CountElements(const ELEMENT& elm, const BoundingBox2D& BB, const size_t z) const noexcept
{
	size_t	Counter = 0, x, y;

	for (y = BB.Y1(); y <= BB.Y2(); y++)
	{
		for (x = BB.X1(); x <= BB.X2(); x++)
		{
			if (GetElement(x, y, z) == elm)
			{
				Counter++;
			}	// if
		}	// for x
	}	// for y

	return Counter;
}	// CountElements

inline ELEMENT Array3D::FindElementTypes(const BoundingBox2D& BB, const size_t z) const noexcept
{
	size_t	i, j;
	ELEMENT	Elm;
	bool	HasEmpty = false, HasFilled = false;

	for (j = BB.Y1(); j <= BB.Y2(); j++)
	{
		for (i = BB.X1(); i <= BB.X2(); i++)
		{
			Elm = GetElement(i, j, z);
			HasEmpty = HasEmpty || (Elm == ELEMENT::ELEMENT_EMPTY);
			HasFilled = HasFilled || (Elm == ELEMENT::ELEMENT_FILLED);

			// Terminate prematurely if there is more than one element type.
			if (HasFilled && HasEmpty) return ELEMENT::ELEMENT_VARIOUS;
		}	// for x
	}	// for y

	if (!HasFilled) return ELEMENT::ELEMENT_EMPTY;
	/*if ( !HasEmpty )*/ return ELEMENT::ELEMENT_FILLED;
}	// FindElementTypes

inline size_t Array3D::CountElements(const ELEMENT& elm, const BoundingBox3D& BB) const noexcept
{
	size_t	Counter = 0, x, y, z;

	for (z = BB.Z1(); z <= BB.Z2(); z++)
	{
		for (y = BB.Y1(); y <= BB.Y2(); y++)
		{
			for (x = BB.X1(); x <= BB.X2(); x++)
			{
				if (GetElement(x, y, z) == elm)
				{
					Counter++;
				}	// if
			}	// for x
		}	// for y
	}	// for z

	return Counter;
}	// CountElements

inline ELEMENT Array3D::FindElementTypes(const BoundingBox3D& BB) const noexcept
{
	size_t	i, j, k;
	ELEMENT	Elm;
	bool	HasEmpty = false, HasFilled = false;

	for (k = BB.Z1(); k <= BB.Z2(); k++)
	{
		for (j = BB.Y1(); j <= BB.Y2(); j++)
		{
			for (i = BB.X1(); i <= BB.X2(); i++)
			{
				Elm = GetElement(i, j, k);
				HasEmpty = HasEmpty || (Elm == ELEMENT::ELEMENT_EMPTY);
				HasFilled = HasFilled || (Elm == ELEMENT::ELEMENT_FILLED);

				// Terminate prematurely of there is more than one element type.
				if (HasFilled && HasEmpty) return ELEMENT::ELEMENT_VARIOUS;
			}	// for x
		}	// for y
	}	// for z

	if (!HasFilled) return ELEMENT::ELEMENT_EMPTY;
	//			if ( !HasEmpty ) return ELEMENT_FILLED;
	return ELEMENT::ELEMENT_FILLED;
}	// FindElementTypes

inline size_t Array3D::GetTotalElements() const noexcept
{
	return PlaneXY * DimZ;
	// return DimX * DimY * DimZ;
}	// GetTotalElements

/*inline size_t Array3D::GetTotalFilledElements() const noexcept
{
	/*size_t TotalElements = GetTotalElements();
	size_t FilledElements{};
	for (size_t i = 0; i < TotalElements; i++)
	{
		if (GetElement(i) == ELEMENT_FILLED)FilledElements++;
	}
	return FilledElements;
	return TotalElements;
}*/

inline BoundingBox3D Array3D::GetBoundingBox3D() const noexcept
{
	return BoundingBox3D((COORD_TYPE)0, (COORD_TYPE)0, (COORD_TYPE)0,
		(COORD_TYPE)(DimX - 1), (COORD_TYPE)(DimY - 1), (COORD_TYPE)(DimZ - 1));
}	// GetBoundingBox3D

void Array3D::PrintArray2D(const BoundingBox2D& BB, const size_t z) const noexcept
{
	size_t	i, j;

	for (j = BB.Y1(); j <= BB.Y2(); j++)
	{
		for (i = BB.X1(); i <= BB.X2(); i++)
		{
			if (GetElement(i, j, z) == ELEMENT::ELEMENT_EMPTY)
				std::cout << "0";
			else
				std::cout << "1";
		}	// for
		std::cout << std::endl;
	}	// for
}	// PrintArray2D

//void PrintArray3D(const BoundingBox3D &BB) const noexcept;
//void PrintArray3D(const BoundingBox3D &BB, const size_t z) const noexcept;

bool Array3D::LoadFromFile(const char* FileSpec) noexcept
{
	FILE* pFile;
	size_t	i, j, k, TotalElements, ElementNumber;

	// 1. Open file.
	pFile = (FILE*)fopen(FileSpec, "r");
	if (pFile == nullptr) // If file doesn't open show an ERROR.
	{
		std::cerr << "Can't open file " << FileSpec << std::endl;
		return false;
	}	// if

	// 2. Load data.
	fscanf(pFile, "%u", &TotalElements);

	//std::cout << "Total elements = " << TotalElements << std::endl;
	//std::cout << "File Opened\n";
	while (!feof(pFile))
	{
		//std::cout << "File Opened\n" ;
		fscanf(pFile, "%u%u%u%u", &ElementNumber, &i, &j, &k);
		//cout << numread << "\t" << i << "\t" << j << "\t" << k << endl;
		////////Array[( k - 1 ) * PlaneXY + ( j - 1 ) * DimX + ( i - 1 )] = ELEMENT_FILLED;
		SetElement(i - 1, j - 1, k - 1, ELEMENT::ELEMENT_FILLED);
	} // while


	// 3. Close file.
	std::fclose(pFile);
	return true;
}	// LoadFromFile

bool Array3D::SaveToFile(const char* FileSpec) noexcept
{
	FILE* pFile;
	size_t	x, y, z, TotalElements, ElementNumber = 1;

	// 1. Open File.
	pFile = (FILE*)fopen(FileSpec, "wt");
	if (pFile == nullptr) // If file doesn't open show an ERROR.
	{
		std::cerr << "Can't open file " << FileSpec << std::endl;
		return false;
	}	// if

	// 2. Save data.
	TotalElements = CountElements(ELEMENT::ELEMENT_FILLED);
	fprintf(pFile, "\n%u\n", TotalElements);
	std::cout << "Total elements = " << TotalElements << std::endl;
	for (z = 0; z < DimZ; z++)
	{
		for (y = 0; y < DimY; y++)
		{
			for (x = 0; x < DimX; x++)
			{
				if (GetElement(x, y, z) == ELEMENT::ELEMENT_FILLED)
				{
					fprintf(pFile, "%u\t%u\t%u\t%u\n", ElementNumber, x + 1, y + 1, z + 1);
					ElementNumber++;
				}	// if
			}	// for x
		}	// for y
	}	// for z

	// 3. Close file.
	fclose(pFile);
	return true;
}	// SaveToFile

inline void Array3D::InitArray3D(const ELEMENT& elm) noexcept
{
	for (size_t k = 0; k < DimZ; k++) // DIMZ
	{
		for (size_t j = 0; j < DimY; j++) // DIMY
		{
			for (size_t i = 0; i < DimX; i++) // DIMX
			{
				//Array[k * DimY * DimX + j * DimX + i] = elm;
				Array[k * PlaneXY + j * DimX + i] = elm;
			}	// for x
		}	// for y
	}	// for z
}	// InitArray3D

inline void Array3D::InitArray3D(const BoundingBox3D& BB, const ELEMENT& elm) noexcept
{
	for (size_t k = BB.Z1(); k <= BB.Z2(); k++) // DIMZ
	{
		for (size_t j = BB.Y1(); j <= BB.Y2(); j++) // DIMY
		{
			for (size_t i = BB.X1(); i <= BB.X2(); i++) // DIMX
			{
				//Array[k * DimY * DimX + j * DimX + i] = elm;
				Array[k * PlaneXY + j * DimX + i] = elm;
			}	// for x
		}	// for y
	}	// for z
}	// InitArray3D

inline const Array3D& Array3D::operator=(const Array3D& op2) noexcept
{
	if (this == &op2)
	{
		return *this;
	} //if

	size_t count;

	DimX = op2.DimX;
	DimY = op2.DimY;
	DimZ = op2.DimZ;
	PlaneXY = op2.PlaneXY;

	free(Array);
	Array = (ELEMENT*)malloc(sizeof(ELEMENT) * DimZ * DimY * DimX);;
	if (Array != nullptr)
	{
		for (count = 0; count < DimX * DimY * DimZ; count++)
		{
			Array[count] = op2.Array[count];
		}	// for
	}//if
	return *this;
}	// operator =

inline bool Array3D::operator==(const Array3D& op2) const noexcept
{
	size_t count;

	if (DimX != op2.DimX || DimY != op2.DimY || DimZ != op2.DimZ)
	{
		return false;
	} //if

	for (count = 0; count < (DimX * DimY * DimZ); count++)
	{
		if (Array[count] != op2.Array[count])
		{
			return false;
		}	// if
	}	// for

	return true;
}	// operator ==

inline void Array3D::Reset() noexcept
{
	for (size_t i = 0; i < DimX * DimY * DimZ; i++)  Array[i] = ELEMENT::ELEMENT_EMPTY;

	/*for (size_t k = 0; k < DimZ; k++)
	{
		for (size_t j = 0; j < DimY; j++)
		{
			for (size_t i = 0; i < DimX; i++)
			{
				SetElement(i, j, k, ELEMENT_EMPTY);
			}	// for
		}	// for
	}	// for*/
}	// Reset