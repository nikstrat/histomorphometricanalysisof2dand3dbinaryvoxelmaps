#pragma once

#include <iostream>
#include <cstdint>
#include <algorithm>
#include <string>
//using namespace std;

/* -------------------------------------------------------------------------- */
/* Bounding-box coordinate type.  Change to alter memory requirements.        */
/* -------------------------------------------------------------------------- */

	typedef		uint16_t	COORD_TYPE;

/* -------------------------------------------------------------------------- */
/* BoundingBox2D class.                                                       */
/* -------------------------------------------------------------------------- */

class BoundingBox2D
{
	private:
		COORD_TYPE	x1{}, y1{}, x2{}, y2{};

	public:
		inline BoundingBox2D(const COORD_TYPE _x1, const COORD_TYPE _y1,
			const COORD_TYPE _x2, const COORD_TYPE _y2) noexcept;	// Constructor
		inline BoundingBox2D(const size_t _x1, const size_t _y1,
			const size_t _x2, const size_t _y2) noexcept;	// Constructor
		inline BoundingBox2D(const size_t DX, const size_t DY) noexcept;// Constructor
		inline BoundingBox2D(const BoundingBox2D& AABB) noexcept;	// Copy constructor
		inline ~BoundingBox2D() noexcept;// Destructor
		inline COORD_TYPE X1() const noexcept;
		inline COORD_TYPE X2() const noexcept;
		inline COORD_TYPE Y1() const noexcept;
		inline COORD_TYPE Y2() const noexcept;
		inline void Get(COORD_TYPE* _x1, COORD_TYPE* _y1,
			COORD_TYPE* _x2, COORD_TYPE* _y2) const noexcept;// Get
		inline void Get(size_t* _x1, size_t* _y1,
			size_t* _x2, size_t* _y2) const noexcept;// Get
		inline void Set(const COORD_TYPE _x1, const COORD_TYPE _y1,
			const COORD_TYPE _x2, const COORD_TYPE _y2) noexcept;// Set
		inline void Set(const size_t _x1, const size_t _y1,
			const size_t _x2, const size_t _y2) noexcept;// Set
		inline size_t Width() const noexcept;
		inline size_t Height() const noexcept;
		inline size_t Size() const noexcept;
		inline void Write(const std::string& Msg) const noexcept;// Write
		inline std::string ToString() const noexcept;// ToString
};

/* -------------------------------------------------------------------------- */
/* BoundingBox3D class.                                                       */
/* -------------------------------------------------------------------------- */
class BoundingBox3D
{
	private:
		COORD_TYPE	x1{}, y1{}, z1{}, x2{}, y2{}, z2{};

	public:
		inline BoundingBox3D(const COORD_TYPE _x1, const COORD_TYPE _y1, const COORD_TYPE _z1,
			const COORD_TYPE _x2, const COORD_TYPE _y2, const COORD_TYPE _z2) noexcept;	// Constructor
		inline BoundingBox3D(const size_t _x1, const size_t _y1, const size_t _z1,
			const size_t _x2, const size_t _y2, const size_t _z2) noexcept;// Constructor
		inline BoundingBox3D(const size_t DX, const size_t DY, const size_t DZ) noexcept;// Constructor
		inline BoundingBox3D(const BoundingBox3D& AABB) noexcept;// Copy constructor
		inline ~BoundingBox3D() noexcept;// Destructor
		inline COORD_TYPE X1() const noexcept;
		inline COORD_TYPE X2() const noexcept;
		inline COORD_TYPE Y1() const noexcept;
		inline COORD_TYPE Y2() const noexcept;
		inline COORD_TYPE Z1() const noexcept;
		inline COORD_TYPE Z2() const noexcept;
		inline void Get(COORD_TYPE* xNear, COORD_TYPE* yNear, COORD_TYPE* zNear,
			COORD_TYPE* xFar, COORD_TYPE* yFar, COORD_TYPE* zFar) const noexcept;// Get
		inline void Get(size_t* _x1, size_t* _y1, size_t* _z1,
			size_t* _x2, size_t* _y2, size_t* _z2) const noexcept;// Get
		inline void Set(const COORD_TYPE xNear, const COORD_TYPE yNear, const COORD_TYPE zNear,
			const COORD_TYPE xFar, const COORD_TYPE yFar, const COORD_TYPE zFar) noexcept;// Set
		inline void Set(const size_t _x1, const size_t _y1, const size_t _z1,
			const size_t _x2, const size_t _y2, const size_t _z2) noexcept;// Set
		inline size_t Width() const noexcept;
		inline size_t Height() const noexcept;
		inline size_t Depth() const noexcept;
		inline size_t Size() const noexcept;
		inline void Write(const std::string& Msg) const noexcept;// Write
		inline std::string ToString() const noexcept;// ToString
};



//--- IMPEMENTATION ---//

//--- BoundingBox2D --- //

BoundingBox2D::BoundingBox2D(const COORD_TYPE _x1, const COORD_TYPE _y1, const COORD_TYPE _x2, const COORD_TYPE _y2) noexcept
	: x1(std::min(_x1, _x2)), y1(std::min(_y1, _y2)), x2(std::max(_x1, _x2)), y2(std::max(_y1, _y2))
{}	// Constructor

BoundingBox2D::BoundingBox2D(const size_t _x1, const size_t _y1, const size_t _x2, const size_t _y2) noexcept
{
	x1 = (COORD_TYPE)std::min(_x1, _x2);
	y1 = (COORD_TYPE)std::min(_y1, _y2);
	x2 = (COORD_TYPE)std::max(_x1, _x2);
	y2 = (COORD_TYPE)std::max(_y1, _y2);
}	// Constructor

BoundingBox2D::BoundingBox2D(const size_t DX, const size_t DY) noexcept
{
	x1 = 0;
	y1 = 0;
	x2 = (COORD_TYPE)DX - 1;
	y2 = (COORD_TYPE)DY - 1;
}	// Constructor

BoundingBox2D::BoundingBox2D(const BoundingBox2D& AABB) noexcept
{
	if (this != &AABB)
	{
		x1 = AABB.x1;
		y1 = AABB.y1;
		x2 = AABB.x2;
		y2 = AABB.y2;
	}	// if
}	// Copy constructor

BoundingBox2D::~BoundingBox2D() noexcept
{
	x1 = x2 = y1 = y2 = 0;
}	// Destructor

inline COORD_TYPE BoundingBox2D::X1() const noexcept { return x1; }

inline COORD_TYPE BoundingBox2D::X2() const noexcept { return x2; }

inline COORD_TYPE BoundingBox2D::Y1() const noexcept { return y1; }

inline COORD_TYPE BoundingBox2D::Y2() const noexcept { return y2; }

inline void BoundingBox2D::Get(COORD_TYPE* _x1, COORD_TYPE* _y1, COORD_TYPE* _x2, COORD_TYPE* _y2) const noexcept
{
	*_x1 = x1;
	*_y1 = y1;
	*_x2 = x2;
	*_y2 = y2;
}	// Get

inline void BoundingBox2D::Get(size_t* _x1, size_t* _y1, size_t* _x2, size_t* _y2) const noexcept
{
	*_x1 = x1;
	*_y1 = y1;
	*_x2 = x2;
	*_y2 = y2;
}	// Get

inline void BoundingBox2D::Set(const COORD_TYPE _x1, const COORD_TYPE _y1, const COORD_TYPE _x2, const COORD_TYPE _y2) noexcept
{
	x1 = std::min(_x1, _x2);
	y1 = std::min(_y1, _y2);
	x2 = std::max(_x1, _x2);
	y2 = std::max(_y1, _y2);
}	// Set

inline void BoundingBox2D::Set(const size_t _x1, const size_t _y1, const size_t _x2, const size_t _y2) noexcept
{
	x1 = (COORD_TYPE)std::min(_x1, _x2);
	y1 = (COORD_TYPE)std::min(_y1, _y2);
	x2 = (COORD_TYPE)std::max(_x1, _x2);
	y2 = (COORD_TYPE)std::max(_y1, _y2);
}	// Set

inline size_t BoundingBox2D::Width() const noexcept { return (size_t)x2 - (size_t)x1 + 1; }

inline size_t BoundingBox2D::Height() const noexcept { return (size_t)y2 - (size_t)y1 + 1; }

inline size_t BoundingBox2D::Size() const noexcept { return Width() * Height(); }

inline void BoundingBox2D::Write(const std::string& Msg) const noexcept
{
	std::cout << "BoundingBox2D object 0x" << this << ": " <<
		Msg.c_str() << "(" << x1 << ", " << y1 << ") -> (" << x2 << ", " << y2 << ")" << std::endl;
}	// Write

inline std::string BoundingBox2D::ToString() const noexcept
{
	return "(" + std::to_string(x1) + ", " + std::to_string(y1) + ") -> (" +
		std::to_string(x2) + ", " + std::to_string(y2) + ")";
}	// ToString


//--- BoundingBox3D --- //


BoundingBox3D::BoundingBox3D(const COORD_TYPE _x1, const COORD_TYPE _y1, const COORD_TYPE _z1,
	const COORD_TYPE _x2, const COORD_TYPE _y2, const COORD_TYPE _z2) noexcept
	: x1(std::min(_x1, _x2)), y1(std::min(_y1, _y2)), z1(std::min(_z1, _z2)),
	x2(std::max(_x1, _x2)), y2(std::max(_y1, _y2)), z2(std::max(_z1, _z2))
{}	// Constructor

BoundingBox3D::BoundingBox3D(const size_t _x1, const size_t _y1, const size_t _z1,
	const size_t _x2, const size_t _y2, const size_t _z2) noexcept
{
	x1 = (COORD_TYPE)std::min(_x1, _x2);
	y1 = (COORD_TYPE)std::min(_y1, _y2);
	z1 = (COORD_TYPE)std::min(_z1, _z2);

	x2 = (COORD_TYPE)std::max(_x1, _x2);
	y2 = (COORD_TYPE)std::max(_y1, _y2);
	z2 = (COORD_TYPE)std::max(_z1, _z2);
}	// Constructor

BoundingBox3D::BoundingBox3D(const size_t DX, const size_t DY, const size_t DZ) noexcept
{
	x1 = 0;
	y1 = 0;
	z1 = 0;

	x2 = (COORD_TYPE)DX - 1;
	y2 = (COORD_TYPE)DY - 1;
	z2 = (COORD_TYPE)DZ - 1;
}	// Constructor

BoundingBox3D::BoundingBox3D(const BoundingBox3D& AABB) noexcept
{
	if (this != &AABB)
	{
		x1 = AABB.x1;
		y1 = AABB.y1;
		z1 = AABB.z1;
		x2 = AABB.x2;
		y2 = AABB.y2;
		z2 = AABB.z2;
	}	// if
}	// Copy constructor

BoundingBox3D::~BoundingBox3D() noexcept
{
	x1 = y1 = z1 = x2 = y2 = z2 = 0;
}	// Destructor

inline COORD_TYPE BoundingBox3D::X1() const noexcept { return x1; }
inline COORD_TYPE BoundingBox3D::X2() const noexcept { return x2; }
inline COORD_TYPE BoundingBox3D::Y1() const noexcept { return y1; }
inline COORD_TYPE BoundingBox3D::Y2() const noexcept { return y2; }
inline COORD_TYPE BoundingBox3D::Z1() const noexcept { return z1; }
inline COORD_TYPE BoundingBox3D::Z2() const noexcept { return z2; }

inline void BoundingBox3D::Get(COORD_TYPE* xNear, COORD_TYPE* yNear, COORD_TYPE* zNear,
	COORD_TYPE* xFar, COORD_TYPE* yFar, COORD_TYPE* zFar) const noexcept
{
	*xNear = x1;
	*yNear = y1;
	*zNear = z1;

	*xFar = x2;
	*yFar = y2;
	*zFar = z2;
}	// Get

inline void BoundingBox3D::Get(size_t* _x1, size_t* _y1, size_t* _z1,
	size_t* _x2, size_t* _y2, size_t* _z2) const noexcept
{
	*_x1 = x1;
	*_y1 = y1;
	*_z1 = z1;

	*_x2 = x2;
	*_y2 = y2;
	*_z2 = z2;
}	// Get

inline void BoundingBox3D::Set(const COORD_TYPE xNear, const COORD_TYPE yNear, const COORD_TYPE zNear,
	const COORD_TYPE xFar, const COORD_TYPE yFar, const COORD_TYPE zFar) noexcept
{
	x1 = std::min(xNear, xFar);
	y1 = std::min(yNear, yFar);
	z1 = std::min(zNear, zFar);

	x2 = std::max(xNear, xFar);
	y2 = std::max(yNear, yFar);
	z2 = std::max(zNear, zFar);
}	// Set

inline void BoundingBox3D::Set(const size_t _x1, const size_t _y1, const size_t _z1,
	const size_t _x2, const size_t _y2, const size_t _z2) noexcept
{
	x1 = (COORD_TYPE)std::min(_x1, _x2);
	y1 = (COORD_TYPE)std::min(_y1, _y2);
	z1 = (COORD_TYPE)std::min(_z1, _z2);

	x2 = (COORD_TYPE)std::max(_x1, _x2);
	y2 = (COORD_TYPE)std::max(_y1, _y2);
	z2 = (COORD_TYPE)std::max(_z1, _z2);
}	// Set

inline size_t BoundingBox3D::Width() const noexcept { return (size_t)x2 - (size_t)x1 + 1; }
inline size_t BoundingBox3D::Height() const noexcept { return (size_t)y2 - (size_t)y1 + 1; }
inline size_t BoundingBox3D::Depth() const noexcept { return (size_t)z2 - (size_t)z1 + 1; }
inline size_t BoundingBox3D::Size() const noexcept { return Width() * Height() * Depth(); }

inline void BoundingBox3D::Write(const std::string& Msg) const noexcept
{
	std::cout << "BoundingBox3D object 0x" << this << ": " <<
		Msg.c_str() << "(" << x1 << ", " << y1 << ", " << z1 << ") -> (" <<
		x2 << ", " << y2 << ", " << z2 << ")" << std::endl;
}	// Write

inline std::string BoundingBox3D::ToString() const noexcept
{
	return "(" + std::to_string(x1) + ", " +
		std::to_string(y1) + ", " +
		std::to_string(z1) + ") -> (" +
		std::to_string(x2) + ", " +
		std::to_string(y2) +
		std::to_string(z2) + ")";
}	// ToString