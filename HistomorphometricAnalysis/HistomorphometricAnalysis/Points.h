#pragma once
#include <iostream>

template<typename T>
class Points3D
{
    private:
        T X{};
        T Y{};
        T Z{};


    public:
        Points3D() noexcept;
        ~Points3D() noexcept;
        Points3D(T x, T Y, T Z) noexcept;

        T& operator=(const T& other)noexcept;
        T& operator==(const T& other)const noexcept;
        T& operator+(const T& other)const noexcept;


        inline void AlterPoints(T Xnew, T Ynew, T Znew) noexcept;

        inline void SetX(T newX)noexcept;
        inline void SetY(T newY)noexcept;
        inline void SetZ(T newZ)noexcept;

        inline T GetX()const noexcept;
        inline T GetY()const noexcept;
        inline T GetZ()const noexcept;

        inline void Print3DPoints()const noexcept;
       
};

template<typename T>
class Points2D
{
    private:
        T X{};
        T Y{};
    public:
        Points2D()noexcept;
        ~Points2D()noexcept;
        T& operator=(const T& other)noexcept;
        T& operator==(const T& other)const noexcept;
        T& operator+(const T& other)const noexcept;

        inline void AlterPoints(T Xnew, T Ynew)noexcept;

        inline void SetX(T newX)noexcept;
        inline void SetY(T newY)noexcept;

        inline T GetX()const noexcept;
        inline T GetY()const noexcept;

        inline void Print2DPoints() const noexcept;
};

// ----------- IMPLEMENTATION ------------//


//------------------ Points3D -------------------//
template<typename T>
inline Points3D<T>::Points3D()noexcept
    :X(0), Y(0), Z(0)
{/**/}//Default Constructor

template<typename T>
inline Points3D<T>::Points3D(T x, T y, T z)noexcept
    : X(x), Y(y), Z(z)
{/**/}//constructor

template<typename T>
inline T& Points3D<T>::operator=(const T & other) noexcept
{
      if (this != &other) 
      { 
          this->X = other.X;
          this->Y = other.Y;
          this->Z = other.Z;
      }
      return *this;
}//operator=

template<typename T>
inline T& Points3D<T>::operator==(const T& other) const noexcept
{
    if (this != &other)
    {
        return ((this->X == other.X) && (this->Y == other.Y) && (this->Z == other.Z));
    }//if
    return false;
}//operator==

template<typename T>
inline T& Points3D<T>::operator+(const T& other) const noexcept
{
    if (this != &other)
    {
        this->X += other.X;
        this->Y += other.X;
        this->Z += other.Z;
    }//if
    return *this;
}//operator+

template<typename T>
inline Points3D<T>::~Points3D()noexcept
{
    X = Y = Z = 0;
}//Destructor

template<typename T>
inline void Points3D<T>::AlterPoints(T Xnew, T Ynew, T Znew)noexcept
{
    X = Xnew;
    Y = Ynew;
    Z = Znew;
}//AlterPoints

template<typename T>
inline void Points3D<T>::SetX(T newX) noexcept { X = newX; }

template<typename T>
inline void Points3D<T>::SetY(T newY) noexcept { Y = newY;}

template<typename T>
inline void Points3D<T>::SetZ(T newZ) noexcept { Z = newZ; }

template<typename T>
inline T Points3D<T>::GetX() const noexcept {return X;}

template<typename T>
inline T Points3D<T>::GetY() const  noexcept { return Y; }

template<typename T>
inline T Points3D<T>::GetZ() const noexcept{ return Z;}

template<typename T>
inline void Points3D<T>::Print3DPoints() const noexcept
{
    std::cout << X << "," << Y << "," << Z << std::endl;
}//Print3DPoints

//----------------- Points2D ----------------------//
template<typename T>
Points2D<T>::Points2D()noexcept
    :X(0), Y(0)
{/**/}

template<typename T>
Points2D<T>::~Points2D()noexcept
{
    X = Y = 0;
}//Destructor

template<typename T>
inline T& Points2D<T>::operator=(const T& other) noexcept
{
    if (this != &other)
    {
        this->X = other.X;
        this->Y = other.Y;
    }//if
    return *this;
}//operator=

template<typename T>
inline T& Points2D<T>::operator==(const T& other) const noexcept
{
    if (this != &other)
    {
        return ((this->X == other.X) && (this->Y == other.X));
    }//if
    return false;
}//operator==

template<typename T>
inline T& Points2D<T>::operator+(const T& other) const noexcept
{
    if (this != &other)
    {
        this->X += other.X;
        this->Y += other.X;
    }//if
    return *this;
}//operator+

template<typename T>
void Points2D<T>::AlterPoints(T Xnew, T Ynew)noexcept
{
    X = Xnew;
    Y = Ynew;
}//AlterPoins

template<typename T>
inline void Points2D<T>::SetX(T newX) noexcept{ X = newX;}

template<typename T>
inline void Points2D<T>::SetY(T newY) noexcept{ Y = newY;}

template<typename T>
inline T Points2D<T>::GetX() const noexcept{ return X;}

template<typename T>
inline T Points2D<T>::GetY() const noexcept{ return Y;}

template<typename T>
inline void Points2D<T>::Print2DPoints() const noexcept
{
    std::cout << X << "," << Y << std::endl;
}//Print2DPoints
