#pragma once
#include <cmath>
#include <iostream>
#include <vector>
#include "Points.h"

constexpr auto PI = 3.1415926535897932384626433;


// ----------- DECLARATION ------------//

inline std::vector<Points3D<int>> Bresenham3D(const int x1, const int y1, const int z1, const int x2, const int y2, const int z2);
inline void Bresenham3D(const int x1, const int y1, const int z1, const int x2, const int y2, const int z2, std::vector<Points3D<int>>& vec);
template<typename T>
inline void Bresenham3D(Points3D<T> A, Points3D<T> B, std::vector<Points2D<int>>& vec);

inline void Bresenham2D(int x1, int y1, int x2, int y2, std::vector<Points2D<int>>& vec);
inline std::vector<Points2D<int>> Bresenham2D(int x1, int y1, int x2, int y2);

inline void Rotateline(Points3D<float>& OldA, Points3D<float>& OldB, float Theta,/*int Axis*/ Points3D<float>& Axis);

inline std::vector<std::vector<float>> Matrix4x4Multiplication(std::vector<std::vector<float>>& Array1, std::vector<std::vector<float>>& Array2);
inline std::vector<float> Matrix4x1Multiplication(std::vector<std::vector<float>>& Array1, std::vector<float>& Array2);
inline void PrintMatrix4x4(std::vector<std::vector<float>>& res);
inline void PrintMatrix4x1(std::vector<float>& res);

inline double Euclidean_distance(Points3D<int> FirstPoint, Points3D<int> SecondPoint) noexcept;

// ----------- IMPLEMENTATION ------------//

///<summary>
///Returns a vector of 3D points containing all the points from (x1,y1,z1) to (x2,y2,z2).
///</summary>
inline std::vector<Points3D<int>> Bresenham3D(const int x1, const int y1, const int z1, const int x2, const int y2, const int z2)//, vector<Points>& vec)
{
    std::vector<Points3D<int>> vec{};
    int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
    int point[3];

    point[0] = x1;
    point[1] = y1;
    point[2] = z1;
    dx = x2 - x1;
    dy = y2 - y1;
    dz = z2 - z1;
    x_inc = (dx < 0) ? -1 : 1;
    l = abs(dx);
    y_inc = (dy < 0) ? -1 : 1;
    m = abs(dy);
    z_inc = (dz < 0) ? -1 : 1;
    n = abs(dz);
    dx2 = l << 1;
    dy2 = m << 1;
    dz2 = n << 1;

    Points3D<int> poin;
    if ((l >= m) && (l >= n))
    {
        err_1 = dy2 - l;
        err_2 = dz2 - l;
        for (i = 0; i < l; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);

            vec.push_back(poin);
            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[1] += y_inc;
                err_1 -= dx2;
            }//if
            if (err_2 > 0)
            {
                point[2] += z_inc;
                err_2 -= dx2;
            }//if
            err_1 += dy2;
            err_2 += dz2;
            point[0] += x_inc;
        }//for
    }//if
    else if ((m >= l) && (m >= n))
    {
        err_1 = dx2 - m;
        err_2 = dz2 - m;
        for (i = 0; i < m; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);

            vec.push_back(poin);
            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[0] += x_inc;
                err_1 -= dy2;
            }//if
            if (err_2 > 0)
            {
                point[2] += z_inc;
                err_2 -= dy2;
            }//if
            err_1 += dx2;
            err_2 += dz2;
            point[1] += y_inc;
        }//for
    }//if
    else
    {
        err_1 = dy2 - n;
        err_2 = dx2 - n;
        for (i = 0; i < n; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);
            vec.push_back(poin);
            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[1] += y_inc;
                err_1 -= dz2;
            }//if
            if (err_2 > 0)
            {
                point[0] += x_inc;
                err_2 -= dz2;
            }//if
            err_1 += dy2;
            err_2 += dx2;
            point[2] += z_inc;
        }//for
    }//if
    poin.AlterPoints(point[0], point[1], point[2]);
    vec.push_back(poin);
    //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point  
    return vec;
}///3d 

///<summary>
///Fills a vector of 2D points containing all the points from (x1,y1,z1) to (x2,y2,z2).
///</summary>
inline void Bresenham3D(const int x1, const int y1, const int z1, const int x2, const int y2, const int z2, std::vector<Points3D<int>>& vec)
{
    //std::vector<Points> vec{};
    int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
    int point[3];

    point[0] = x1;
    point[1] = y1;
    point[2] = z1;
    dx = x2 - x1;
    dy = y2 - y1;
    dz = z2 - z1;
    x_inc = (dx < 0) ? -1 : 1;
    l = abs(dx);
    y_inc = (dy < 0) ? -1 : 1;
    m = abs(dy);
    z_inc = (dz < 0) ? -1 : 1;
    n = abs(dz);
    dx2 = l << 1;
    dy2 = m << 1;
    dz2 = n << 1;

    Points3D<int> poin;
    if ((l >= m) && (l >= n))
    {
        err_1 = dy2 - l;
        err_2 = dz2 - l;
        for (i = 0; i < l; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);
            vec.push_back(poin);//
            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[1] += y_inc;
                err_1 -= dx2;
            }//if
            if (err_2 > 0)
            {
                point[2] += z_inc;
                err_2 -= dx2;
            }//if
            err_1 += dy2;
            err_2 += dz2;
            point[0] += x_inc;
        }//for
    }//if
    else if ((m >= l) && (m >= n))
    {
        err_1 = dx2 - m;
        err_2 = dz2 - m;
        for (i = 0; i < m; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);
            vec.push_back(poin);//

            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[0] += x_inc;
                err_1 -= dy2;
            }//if
            if (err_2 > 0)
            {
                point[2] += z_inc;
                err_2 -= dy2;
            }//if
            err_1 += dx2;
            err_2 += dz2;
            point[1] += y_inc;
        }//for
    }//if
    else
    {
        err_1 = dy2 - n;
        err_2 = dx2 - n;
        for (i = 0; i < n; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);
            vec.push_back(poin);//
            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[1] += y_inc;
                err_1 -= dz2;
            }//if
            if (err_2 > 0)
            {
                point[0] += x_inc;
                err_2 -= dz2;
            }//if
            err_1 += dy2;
            err_2 += dx2;
            point[2] += z_inc;
        }//for
    }//if
    poin.AlterPoints(point[0], point[1], point[2]);
    vec.push_back(poin);//
    //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point  
    
}///3d 

///<summary>
///Fills a vector of 2D points containing all the points from point A(x,y,z) to point B(x,y,z).
///</summary>
template<typename T>
inline void Bresenham3D(Points3D<T> A, Points3D<T> B, std::vector<Points3D<int>>& vec)
{
    //std::vector<Points> vec{};
    int i, dx, dy, dz, l, m, n, x_inc, y_inc, z_inc, err_1, err_2, dx2, dy2, dz2;
    int point[3];

    /*point[0] = x1;
    point[1] = y1;
    point[2] = z1;*/
    point[0] = A.GetX();
    point[1] = A.GetY();
    point[2] = A.GetZ();
    dx = B.GetX() - A.GetX();
    dy = B.GetY() - A.GetY();
    dz = B.GetZ() - A.GetZ();
    x_inc = (dx < 0) ? -1 : 1;
    l = abs(dx);
    y_inc = (dy < 0) ? -1 : 1;
    m = abs(dy);
    z_inc = (dz < 0) ? -1 : 1;
    n = abs(dz);
    dx2 = l << 1;
    dy2 = m << 1;
    dz2 = n << 1;

    Points3D<int> poin;
    if ((l >= m) && (l >= n))
    {
        err_1 = dy2 - l;
        err_2 = dz2 - l;
        for (i = 0; i < l; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);
            //vec.push_back(poin);//
            vec.push_back(poin);
            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[1] += y_inc;
                err_1 -= dx2;
            }//if
            if (err_2 > 0)
            {
                point[2] += z_inc;
                err_2 -= dx2;
            }//if
            err_1 += dy2;
            err_2 += dz2;
            point[0] += x_inc;
        }//for
    }//if
    else if ((m >= l) && (m >= n))
    {
        err_1 = dx2 - m;
        err_2 = dz2 - m;
        for (i = 0; i < m; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);
            vec.push_back(poin);//

            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[0] += x_inc;
                err_1 -= dy2;
            }//if
            if (err_2 > 0)
            {
                point[2] += z_inc;
                err_2 -= dy2;
            }//if
            err_1 += dx2;
            err_2 += dz2;
            point[1] += y_inc;
        }//for
    }//if
    else
    {
        err_1 = dy2 - n;
        err_2 = dx2 - n;
        for (i = 0; i < n; i++)
        {
            poin.AlterPoints(point[0], point[1], point[2]);
            vec.push_back(poin);//
            //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point 
            if (err_1 > 0)
            {
                point[1] += y_inc;
                err_1 -= dz2;
            }//if
            if (err_2 > 0)
            {
                point[0] += x_inc;
                err_2 -= dz2;
            }//if
            err_1 += dy2;
            err_2 += dx2;
            point[2] += z_inc;
        }//for
    }//if
    poin.AlterPoints(point[0], point[1], point[2]);
    vec.push_back(poin);//
    //std::cout << point[0] << "," << point[1] << "," << point[2] << std::endl;//prints the algorithm point  

}//bresenham3D

///<summary>
///Fills a vector of 2D points containing all the points from (x1,y1) to (x2,y2).
///</summary>
inline void Bresenham2D(int x1, int y1, int x2, int y2, std::vector<Points2D<int>>& vec)
{
    Points2D<int> poin;
    int m_new = 2 * (y2 - y1);
    int slope_error_new = m_new - (x2 - x1);
    for (int x = x1, y = y1; x <= x2; x++)
    {
        poin.AlterPoints(x, y);
        
        vec.push_back(poin);
        //std::cout << "(" << x << "," << y << ")\n";//prints the algorithm point 

        // Add slope to increment angle formed 
        slope_error_new += m_new;

        // Slope error reached limit, time to 
        // increment y and update slope error. 
        if (slope_error_new >= 0)
        {
            y++;
            slope_error_new -= 2 * (x2 - x1);
        }//if
    }//for
}//2d

///<summary>
///Returns a vector of 2D points containing all the points from (x1,y1) to (x2,y2).
///</summary>
inline std::vector<Points2D<int>> Bresenham2D(int x1, int y1, int x2, int y2)
{
    std::vector<Points2D<int>> vec;
    int m_new = 2 * (y2 - y1);
    int slope_error_new = m_new - (x2 - x1);
    Points2D<int> poin;
    for (int x = x1, y = y1; x <= x2; x++)
    {
        
        poin.AlterPoints(x, y);
        vec.push_back(poin);//
        //std::cout << "(" << x << "," << y << ")\n";//prints the algorithm point 

        // Add slope to increment angle formed 
        slope_error_new += m_new;

        // Slope error reached limit, time to 
        // increment y and update slope error. 
        if (slope_error_new >= 0)
        {
            y++;
            slope_error_new -= 2 * (x2 - x1);
        }//if
    }//for
    return vec;
}//2d

///<summary>
///Rotate a line defined by 2 3d points by an angle Theta and a unit vector that specifies the axis (x, y or z).
///</summary>
inline void Rotateline(Points3D<float>& OldA, Points3D<float>& OldB, float Theta, /* int Axis*/Points3D<float>& Axis)
{
    Points3D<float> middle, newA, newB;
    Theta = (float)((Theta * PI) / 180.0);
    float ccos = cos(Theta);
    float ssin = sin(Theta);
    float AxisX = Axis.GetX();
    float AxisY = Axis.GetY();
    float AxisZ = Axis.GetZ();

    float AxisXY = AxisX * AxisY;
    float AxisXZ = AxisX * AxisZ;
    float AxisYZ = AxisY * AxisZ;

    //we calculate the middle of the line
    middle.AlterPoints(((OldA.GetX() + OldB.GetX()) / 2), ((OldA.GetY() + OldB.GetY()) / 2), ((OldA.GetZ() + OldB.GetZ()) / 2));

    //then we substract the points from the middle so the middle aligns with the origin m(0,0,0)
    newA.AlterPoints((OldA.GetX() - middle.GetX()), (OldA.GetY() - middle.GetY()), (OldA.GetZ() - middle.GetZ()));
    newB.AlterPoints((OldB.GetX() - middle.GetX()), (OldB.GetY() - middle.GetY()), (OldB.GetZ() - middle.GetZ()));

    /* rotation matrix used */

//{ cos + Ux^2*(1-cos)        Ux*Uy*(1-cos) - Uz*sin        Ux*Uz*(1-cos)+Uz*sin  }

//{ Uy*Ux*(1-cos)+Uz*sin       cos+ Uy^2*(1-cos)             Uy*Uz*(1-cos) - Ux*sin}

//{ Uz*Ux*(1-cos)-Uy*sin       Uz*Uy*(1-cos)+Ux*sin          cos+Uz^2*(1-cos)      }

    float Ax = newA.GetX(),
        Ay = newA.GetY(),
        Az = newA.GetZ();

    float Bx = newB.GetX(),
        By = newB.GetY(),
        Bz = newB.GetZ();

    //rotate and translate point A
    OldA.SetX(((ccos + pow(AxisX, 2) * (1 - ccos)) * Ax) +
        ((AxisXY * (1 - ccos) - AxisZ * ssin) * Ay) +
        ((AxisXZ * (1 - ccos) + AxisY * ssin) * Az) + middle.GetX());

    OldA.SetY(((AxisXY * (1 - ccos) + AxisZ * ssin) * Ax) +
        ((ccos + pow(AxisY, 2) * (1 - ccos)) * Ay) +
        ((AxisYZ * (1 - ccos) - AxisX * ssin) * Az) + middle.GetY());

    OldA.SetZ(((AxisXZ * (1 - ccos) - AxisY * ssin) * Ax) +
        ((AxisYZ * (1 - ccos) + AxisX * ssin) * Ay) +
        ((ccos + pow(AxisZ, 2) * (1 - ccos)) * Az) + middle.GetZ());

    //rotate and translate point B
    OldB.SetX(((ccos + pow(AxisX, 2) * (1 - ccos)) * Bx) +
        ((AxisXY * (1 - ccos) - AxisZ * ssin) * By) +
        ((AxisXZ * (1 - ccos) + AxisY * ssin) * Bz) + middle.GetX());

    OldB.SetY(((AxisXY * (1 - ccos) + AxisZ * ssin) * Bx) +
        ((ccos + pow(AxisY, 2) * (1 - ccos)) * By) +
        ((AxisYZ * (1 - ccos) - AxisX * ssin) * Bz) + middle.GetY());

    OldB.SetZ(((AxisXZ * (1 - ccos) - AxisY * ssin) * Bx) +
        ((AxisYZ * (1 - ccos) + AxisX * ssin) * By) +
        ((ccos + pow(AxisZ, 2) * (1 - ccos)) * Bz) + middle.GetZ());



}//RotateLine

///<summary>
///A simple function that multiplies a 4 x 4 matrix with a 4 x 4 one. 
///</summary>
inline std::vector<std::vector<float>> Matrix4x4Multiplication(std::vector<std::vector<float>> &Array1, std::vector<std::vector<float>>&Array2)
{
    size_t i{}, j{}, k{};
    std::vector<std::vector<float> > res(4, std::vector<float>(4));
    
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 4; k++)
            {
                res[i][j] += Array1[i][k] * Array2[k][j];
            }//for
        }//for
    }//for
    return res;
}//Matrix4x4Multiplication

///<summary>
///A simple function that multiplies a 4 x 4 matrix with a 4 x 1 one. 
///</summary>
inline std::vector<float> Matrix4x1Multiplication(std::vector<std::vector<float>>& Array1, std::vector<float> &Array2)
{
    size_t i{}, j{}, k{};
    std::vector<float> res(4,0);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            res[i] += Array1[i][j] * Array2[j];
        }//for
    }//for
    return res;
}//Matrix4x1Multiplication

///<summary>
///Prints a 4 x 4 matrix on screen.
///</summary>
inline void PrintMatrix4x4(std::vector<std::vector<float>>& res)
{
    std::cout << res[0][0] << " " << res[0][1] << " " << res[0][2] << " " << res[0][3] << std::endl;
    std::cout << res[1][0] << " " << res[1][1] << " " << res[1][2] << " " << res[1][3] << std::endl;
    std::cout << res[2][0] << " " << res[2][1] << " " << res[2][2] << " " << res[2][3] << std::endl;
    std::cout << res[3][0] << " " << res[3][1] << " " << res[3][2] << " " << res[3][3] << std::endl;
}//print4x4

///<summary>
///Prints a 4 x 1 matrix on screen.
///</summary>
inline void PrintMatrix4x1(std::vector<float>& res)
{
    std::cout << res[0] << std::endl;
    std::cout << res[1] << std::endl;
    std::cout << res[2] << std::endl;
    std::cout << res[3] << std::endl;
}

///<summary>
///Returns the euclidean distance of two 3D points
///</summary>
inline double Euclidean_distance(Points3D<int> FirstPoint, Points3D<int> SecondPoint) noexcept
{
    return (sqrt((pow((FirstPoint.GetX() - SecondPoint.GetX()), 2)) +
        (pow((FirstPoint.GetY() - SecondPoint.GetY()), 2)) +
        (pow((FirstPoint.GetZ() - SecondPoint.GetZ()), 2))));
   
}
