#pragma once
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>

#include "Array3D.h"
#include "Points.h"
#include "BoundingBox.h"
#include "utilities.h"

//using namespace std;

// -------------- D�CLARATION -------------------//

inline void Tb(Array3D* Array);
inline void Tb_ThSp�(std::vector<Points3D<int>>& vec, const Array3D* Array, size_t& sumoffilled, size_t& sumofempty, size_t& Thickness, size_t& space);

// --------------IMPLEMENTATION ---------------- //

inline void Tb(Array3D* Array)
{
    size_t DimX = Array->GetDimX(),
        DimY = Array->GetDimY(),
        DimZ = Array->GetDimZ(), z{}, y{};
    std::vector<Points3D<int>> vec(ceil(sqrt(3) * DimZ) * sizeof(Points3D<int>));//reserve enough space to avoid unnecessary memory reallocations
    size_t sumfilled{}, //the number of trabeculae 
        sumempty{}, //the number of empty trabeculae(space between two trabeculae)
        thickness{}, //the total thickness of all the trabeculae
        space{}; //the total space between two trabeculae

    Points3D<float> A, B,
        AxisZ(0, 0, 1.0f),
       // AxisX(1.0f, 0, 0),
        AxisY(0, 1.0f, 0);
    size_t degrees = 180;//the degrees we are going to rotate our line 
    /*

    We start our line from (0,0,0) -> to (DimX,DimY,DimZ) and rotate it on the 
    z axis by 1 degree and calculate  Tb_Th Tb_Sp and Tb_N.

    When we finish our first slice we rotate our line on the y axis also by 1 degree
    and calculate tb_Th tb_Sp and tb_N.

    We do this for 180 degrees on these two axis which means we created a half spheere 


              (0,DimY,Dimz)x---------------------------------x(Dimx,DimY,Dimz)
                         x |                               x |
                       x   |                             x   |
                     x     |                           x     |
                   x       |                         x       |
                 x         |                       x         |
               x           |                     x           |
             x             |                   x             |
    (0,DimY,0)-------------------------------x(DimX,DimY,0)  |
             |             |                 |               |
             |             |                 |               |
             |             |                 |               |
             |             |                 |               |
             |             |                 |               |
             |    (0,0,DimZ------------------|---------------(DimX,0,DimZ)
             |            x                  |             x
             |          x                    |           x
             |        x                      |         x
             |      x                        |       x
             |    x                          |     x
             |  x                            |   x
             |x                              | x
       (0,0,0)-------------------------------x(DimX,0,0)
    */

      
    A.AlterPoints(0, 0, 0);
    B.AlterPoints(DimX, DimY, DimZ);
    for (z = 0; z < degrees; z++)
    {
        Rotateline(A, B, 1.0f, AxisY);

        for (y = 0; y < degrees; y++)
        {
            Rotateline(A, B, 1.0f, AxisZ);

            A.AlterPoints(floor(A.GetX()), floor(A.GetY()), floor(A.GetZ()));
            B.AlterPoints(ceil(B.GetX()), ceil(B.GetY()), ceil(B.GetZ()));

            //call the Bresenham algorithm to generate a straight line
            //from A -> B and save the points inside a vector 
            Bresenham3D(A, B, vec);

            //traverse the vector and count Tb_th, Tb_N, Tb_Sp
            Tb_ThSp�(vec, Array, sumfilled, sumempty, thickness, space);

            vec.clear();
        }//for
    }//for z
    
    //Results 
    std::ofstream ofs("Results.txt", std::ofstream::out);

    ofs << "Total sample thickness = " << thickness << std::endl;
    ofs << "Number of trabeculae: " << sumfilled << std::endl;
    ofs << "Mean Tb.th: " << (float)thickness / sumfilled << std::endl << std::endl;

    ofs << "Total space between trabeculae: " << space << std::endl;
    ofs << "Number of empty trabeculae: " << sumempty << std::endl;
    ofs << "mean Tb.Sp: " << (float)space / sumempty << std::endl << std::endl;

    ofs.close();
    /*std::cout << "Total Thickness = " << thickness << std::endl;
    std::cout << "Number of trabeculae: " << sumfilled << std::endl;
    std::cout << "mean Tb.th: " << (float)thickness / sumfilled << std::endl << std::endl;

    std::cout << "Total space: " << space << std::endl;
    std::cout << "Number of empty trabeculae: " << sumempty << std::endl;
    std::cout << "mean Tb.Sp: " << (float)space / sumempty << std::endl << std::endl;*/
}//Tb


inline void Tb_ThSp�(std::vector<Points3D<int>>& vec, const Array3D* Array, size_t& sumoffilled, size_t& sumofempty, size_t& Thickness, size_t& space)
{
    size_t i{}, VecSize = vec.size() - 1;

    bool in_out = false,
        out = false;
    Points3D<int> inside3DPoint, outside3DPoint;

    for (i = 0; i < VecSize; i++)
    {
        //first check if the current point in our vector is outside of the sample dimensions 
        //or bolow 0 in every Axis
        if (((vec[i].GetX() >= 0) && (vec[i].GetX() <= Array->GetDimX()-1)) && ((vec[i].GetY() >= 0) && (vec[i].GetY() <= Array->GetDimY()-1)) && ((vec[i].GetZ() >= 0) && (vec[i].GetZ() <= Array->GetDimZ()-1)))
        {
            //Traverse the vector until we find a filled voxel
            if ((in_out == false) && (Array->GetElement(vec[i].GetX(), vec[i].GetY(), vec[i].GetZ()) == ELEMENT::ELEMENT_FILLED))
            {
                // mark our entry point in the trabeculae
                inside3DPoint.AlterPoints(vec[i].GetX(), vec[i].GetY(), vec[i].GetZ());
                in_out = true;

                //compute the euclidean distance of the trabecular space between two trabeculae
                //if and only if we exited a trabeculae and we entered another one 
                if (out == true)
                {
                    space += (size_t)Euclidean_distance(outside3DPoint, vec[i]);
                    sumofempty++;

                    outside3DPoint.AlterPoints(0, 0, 0);
                    out = false;
                }//if
            }//if

            //compute the euclidean distance of the current trabeculae
            //if and only if we exited one
            else if ((in_out == true) && (Array->GetElement(vec[i].GetX(), vec[i].GetY(), vec[i].GetZ()) == ELEMENT::ELEMENT_EMPTY))
            {
                Thickness += (size_t)Euclidean_distance(inside3DPoint, vec[i]);
                sumoffilled++;
             
                in_out = false;
                inside3DPoint.AlterPoints(0, 0, 0);

                //this is the first point of empty trebacular space 
                outside3DPoint.AlterPoints(vec[i].GetX(), vec[i].GetY(), vec[i].GetZ());

                //we make this true in order to calculate 
                //the euclidean distance of the space between 2 trabeculae 
                out = true;
            }//if
        }//if
    }//for
    
    //close any loose ends for example we had a filled voxel in the last vector element
    // or we didnt find any void element to compute euclidean distance 
    if (in_out == true)
    {
        Thickness += (size_t)Euclidean_distance(inside3DPoint, vec[i]);
        sumoffilled++;
        in_out = false;
    }//if
    in_out = false;
}//Tb_ThSp

// ------------------------------------------- //
