#include <cmath>
#include <iostream>
#include <chrono> 
#include "Array3D.h"
#include "TrabecularAnalysis.h"

using namespace std;


int main()
{  
     Array3D* Array = new Array3D(143, 143, 143);
     Array->LoadFromFile("C:\\Dev\\histomorphometricanalysisof2dand3dbinaryvoxelmaps\\Human Samples\\CAB_36_DATA_SHORT_ISLAND.txt");
   
     //auto start = std::chrono::steady_clock::now();
   
     Tb(Array);
     cout << "Success";

     //auto end = std::chrono::steady_clock::now();
     //std::cout << "Elapsed time in seconds : "
        // << std::chrono::duration_cast<chrono::milliseconds>(end - start).count() << " secs" << endl;
    
     delete Array;

     return 0;
}//main